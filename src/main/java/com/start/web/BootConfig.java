package com.start.web;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Component
public class BootConfig implements ApplicationContextAware, WebApplicationInitializer {


    private static ApplicationContext context;
    private DispatcherServlet dispatcherServlet ;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
        // this.dispatcherServlet = applicationContext.getBean(DispatcherServlet.class);
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        ServletRegistration.Dynamic dynamic = servletContext.addServlet("dispatcherServlet",context.getBean(DispatcherServlet.class));
        dynamic.setLoadOnStartup(1);
        dynamic.addMapping("/");
    }
}
