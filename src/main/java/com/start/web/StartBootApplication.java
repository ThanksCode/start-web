package com.start.web;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;

@Configurable
@Import(value = DispatcherServlet.class)
@ComponentScan("com.start.web")
public class StartBootApplication {

    public static void main(String[] args) throws ServletException, LifecycleException {
        // 初始化一个容器
        AnnotationConfigWebApplicationContext config = new AnnotationConfigWebApplicationContext();
        // 手动去注册一个bean
        config.register(StartBootApplication.class);
        // 刷新
        config.refresh();
        // 构建Tomcat容器
        Tomcat tomcat=new Tomcat();
        tomcat.setPort(8080);
        tomcat.addWebapp("/","D://mall");
        tomcat.start();
        //因为  tomcat.start();是非阻塞型的，所以要阻塞一下，不能让服务停止。
        tomcat.getServer().await();
    }


}
